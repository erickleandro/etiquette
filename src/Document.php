<?php


namespace Etiquette;


class Document
{
    public function __construct(Definition $definition, \Twig_Environment $renderEngine = null)
    {
        $this->definition = $definition;
        $this->renderEngine = $renderEngine == null
            ? new \Twig_Environment(new \Twig_Loader_Filesystem(__DIR__))
            : $renderEngine;
    }

    public function generate(array $labels, $modelo)
    {
        return $this->renderEngine->render("{$modelo}.html.twig", [
            'definition' => $this->definition,
            'labels' => $this->partitionLabels($labels),
        ]);
    }

    private function partitionLabels(array $labels)
    {
        $rows = $this->partitionBy($labels, $this->definition->labelsPerRow());
        return $this->partitionBy($rows, $this->definition->rowsPerPage());
    }

    /**
     * Partitions a collection by a numeric index leaving the remaining items in the last partition.
     *
     * Ex: partitionBy([1,2,3,4,5,6,7,8,9,10], 2) => [[1,2], [3,4], [5,6], [7,8], [9,10]]
     *
     * @param array $coll
     * @param $n
     * @return array
     */
    private function partitionBy(array $coll, $n)
    {
        list(, $result) = array_reduce($coll, function ($acc, $item) use ($n) {
            list($index, $state) = $acc;

            if ($index % $n == 0) {
                array_push($state, []);
            }

            $newEnd = end($state);
            array_push($newEnd, $item);
            array_pop($state);
            array_push($state, $newEnd);

            return [$index + 1, $state];
        }, [0, []]);

        return $result;
    }
}