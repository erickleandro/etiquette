<?php


namespace Etiquette\Test;


use Etiquette\Definition;
use Etiquette\Document;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class DocumentTest extends \PHPUnit_Framework_TestCase
{
    /** @var Definition */
    private $definition;
    /** @var ObjectProphecy */
    private $renderEngine;
    /** @var Document */
    private $document;

    public function setUp()
    {
        $this->definition = new Definition();
        $this->renderEngine = $this->prophesize('\Twig_Environment');
        $this->document = new Document($this->definition, $this->renderEngine->reveal());
    }

    public function test_generate_returns_string()
    {
        $this->renderEngine->render(Argument::any(), Argument::any())->willReturn('');

        $this->assertInternalType('string', $this->document->generate([]));
    }

    public function test_labels_are_passed_to_render_in_pages_and_rows()
    {
        $labels = [
            [
                [
                    ['label 1 line 1'],
                    ['label 2 line 1'],
                    ['label 3 line 1'],
                ],
                [
                    ['label 4 line 2'],
                    ['label 5 line 2'],
                    ['label 6 line 2'],
                ]
            ]
        ];

        $this->renderEngine->render(Argument::any(), Argument::withEntry('labels', $labels))->shouldBeCalled();

        $this->document->generate([
            ['label 1 line 1'],
            ['label 2 line 1'],
            ['label 3 line 1'],
            ['label 4 line 2'],
            ['label 5 line 2'],
            ['label 6 line 2'],
        ]);
    }

    public function test_odd_numbers_of_labels_are_passed_to_render_in_rows()
    {
        $labels = [
            [
                [
                    ['label 1 line 1'],
                    ['label 2 line 1'],
                    ['label 3 line 1'],
                ],
                [
                    ['label 4 line 2'],
                    ['label 5 line 2'],
                    ['label 6 line 2'],
                ],
                [
                    ['label 7 line 3'],
                ]
            ]
        ];

        $this->renderEngine->render(Argument::any(), Argument::withEntry('labels', $labels))->shouldBeCalled();

        $this->document->generate([
            ['label 1 line 1'],
            ['label 2 line 1'],
            ['label 3 line 1'],
            ['label 4 line 2'],
            ['label 5 line 2'],
            ['label 6 line 2'],
            ['label 7 line 3'],
        ]);
    }

    public function test_pages_partition_works_correctly()
    {
        $labels = [
            [
                [
                    ['page 1 label 1'],
                    ['page 1 label 2']
                ],
            ],
            [
                [
                    ['page 2 label 1'],
                    ['page 2 label 2']
                ],
            ],
        ];

        $definition = new Definition();
        $definition->setDimensions([200, 1000])
            ->setLabelWidth(100)
            ->setLabelHeight(1000);
        $document = new Document($definition, $this->renderEngine->reveal());

        $this->renderEngine->render(Argument::any(), Argument::withEntry('labels', $labels))->shouldBeCalled();
        $document->generate([
            ['page 1 label 1'],
            ['page 1 label 2'],
            ['page 2 label 1'],
            ['page 2 label 2'],
        ]);
    }
}
