<?php

require __DIR__ . '/vendor/autoload.php';

$label = [
    ' In laoreet aliquam dolor sit.',
    'Curabitur velit neque, faucibus.',
    'Integer nec libero tempus.',
];
$labels = array_fill(0, 60, $label);

$definition = new \Etiquette\Definition();
$definition->setTopMargin(12)
    ->setBottomMargin(0)
    ->setLeftMargin(7.9)
    ->setRightMargin(7.9);

$twigLoader = new \Twig_Loader_Filesystem(__DIR__ . '/src');
$renderEngine = new Twig_Environment($twigLoader);
$renderEngine->addFunction(new Twig_SimpleFunction('dump', function ($var) {
    return print_r($var, true);
}));

$document = new \Etiquette\Document($definition, $renderEngine);
$rendered = $document->generate($labels);
//file_put_contents('/home/eduardo/test.html', $rendered);exit;
//echo $rendered;exit;

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml($rendered);
$dompdf->render();
$dompdf->stream('etiquette.pdf', ['Attachment' => 0]);
